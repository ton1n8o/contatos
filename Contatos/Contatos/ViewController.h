//
//  ViewController.h
//  Contatos
//
//  Created by Antônio Carlos on 6/17/14.
//  Copyright (c) 2014 TNTStudios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormViewController.h"

#import "Contato.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, FormViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
