//
//  FormViewController.h
//  Contatos
//
//  Created by Antônio Carlos on 8/28/14.
//  Copyright (c) 2014 TNTStudios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Contato.h"

@protocol FormViewControllerDelegate <NSObject>

@required
-(void)contatoSalvo:(Contato*) contato;

@end

@interface FormViewController : UIViewController

@property(nonatomic,strong) id<FormViewControllerDelegate> delegate;

// Elementos da View
@property (strong, nonatomic) IBOutlet UITextField *edtNome;
@property (strong, nonatomic) IBOutlet UITextField *edtTelefone;
@property (strong, nonatomic) IBOutlet UITextField *edtEmail;

// Ações
- (IBAction)actionSalvar:(id)sender;

@end
