//
//  FormViewController.m
//  Contatos
//
//  Created by Antônio Carlos on 8/28/14.
//  Copyright (c) 2014 TNTStudios. All rights reserved.
//

#import "FormViewController.h"

@interface FormViewController ()

@end

@implementation FormViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionSalvar:(id)sender {
    
    // validar campos...
    
    Contato *c = [Contato new];
    
    c.nome = _edtNome.text;
    c.telefone = _edtTelefone.text;
    c.email = _edtEmail.text;
    c.foto = [UIImage imageNamed:@"contato1"];
    
    // retornamos...
    [_delegate contatoSalvo:c];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
