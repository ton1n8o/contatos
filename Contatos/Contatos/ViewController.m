//
//  ViewController.m
//  Contatos
//
//  Created by Antônio Carlos on 6/17/14.
//  Copyright (c) 2014 TNTStudios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(nonatomic, strong) NSArray *searchData; // registros filtrados
@property(nonatomic, strong) NSMutableArray* data; // registros


@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

#pragma mark - UISearchDisplayDelegate

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContents:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

-(void)filterContents:(NSString*)searchTerm scope:(NSString*)scope
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nome contains[c] %@", searchTerm];
    self.searchData =   [_data filteredArrayUsingPredicate:predicate];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchDisplayController.active) {
        return self.searchData.count;
    } else {
        return self.data.count;
    }
}

#pragma mark - UITabelViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID  = @"IDCell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    UIImageView *imgView = (UIImageView*) [cell viewWithTag:10];
    UILabel *lblName = (UILabel*) [cell viewWithTag:20];
    
    Contato *c;
    
    if (self.searchDisplayController.active) {
        c = [self.searchData objectAtIndex:indexPath.row];
    } else {
        c = [self.data objectAtIndex:indexPath.row];
    }
    
    lblName.text = c.nome;
    imgView.image = c.foto;
    
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    imgView.layer.masksToBounds = YES;
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    FormViewController* formView =  segue.destinationViewController;
    formView.delegate = self;
}

#pragma mark - FormViewControllerDelegate

-(void)contatoSalvo:(Contato*)contato
{
    
    if (_data == nil) {
        _data = [[NSMutableArray alloc] init];
    }
    [_data addObject: contato];
    
    // atualiazamos a tableView
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
