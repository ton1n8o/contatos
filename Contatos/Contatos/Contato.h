//
//  Contato.h
//  Contatos
//
//  Created by Antônio Carlos on 8/28/14.
//  Copyright (c) 2014 TNTStudios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contato : NSObject

@property(nonatomic,strong) NSString *nome;
@property(nonatomic,strong) NSString *telefone;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) UIImage *foto;

@end
