//
//  AppDelegate.h
//  Contatos
//
//  Created by Antônio Carlos on 6/17/14.
//  Copyright (c) 2014 TNTStudios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
